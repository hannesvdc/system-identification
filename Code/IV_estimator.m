function R_IV = IV_estimator(i, u, s)
% IV_ESTIMATOR Compute the instrument- variables estimator for the resistor 
% problem.
%   R_IV = IV_ESTIMATOR(i, u, s) computes the
%   IV-estimator R_IV, based on input i, output u, and shift s.
%
%   Note: we shift forward in time.
    u_shifted = [u((s+1):end) ; zeros(s,1)];
    i_shifted = [i((s+1):end) ; zeros(s, 1)];
    R_IV = (u_shifted' * i) / ( i_shifted' * i);
end