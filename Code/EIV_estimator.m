function R_EIV = EIV_estimator(i, u, sigma_ni, sigma_nu)
% EIV_ESTIMATOR Compute the errors-in-variables estimator for the resistor 
% problem.
%   R_EIV = EIV_ESTIMATOR(i, u, sigma_ni, sigma_nu) computes the
%   EIV-estimator R_EIV, based on input i, output u, standard deviation on
%   the input noise sigma_ni and on the output noise sigma_nu.
    u_sqc = u'*u/sigma_nu^2;
    i_sqc = i'*i/sigma_ni^2;
    R_EIV = (u_sqc - i_sqc + sqrt((u_sqc - i_sqc)^2 + 4*(u'*i)^2/(sigma_ni^2*sigma_nu^2)))/(2*(u'*i)/sigma_nu^2);
end