This folder contains all the code for the first report. More specifically:

    LS_estimator.m :        General linear least squares estimator
    IV_estimator.m :        General Instrumental Variables estimator
    EIV_estimator.m:        General Errors-In-Variables estimator

    data_generator.m:       Function used to generate preprocessed data and noise for the LS and IV estimator.
    data_generator_eIV.m:   Data generator used to for the EIV experiments

    IV_method_2121.m:       Script that performs all the experiment and makes all the plots for the comparison
                            between the LS and IV estimators. More specifically: histograms, PDF's, frequency
                            responses, plots of the autocorrelation, ..
                            This contains all the code for the first experiment where we consider 1 shift paramter
                            s = 1 and differenct cut-off frequencies.

    IV_method_2122.m        Script used to carry out all the numerical experiments for the second experiment where
                            we compare LS with IV for different shift parameters. More specifically: Histograms, 
                            PDF's, autocorrelation, frequency response, ..