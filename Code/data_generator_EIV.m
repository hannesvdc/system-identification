function [i, u, ni, nu, i0, u0] = data_generator_EIV(N, sigma_i0, sigma_ni, sigma_nu, R0)
% DATA_GENERATOR_EIV Generates data as described in the experiments.
%   [i, u, ni, nu, i0, u0] = DATA_GENERATOR(N, sigma_i0, sigma_ni, sigma_nu, R0) 
%   generates measured currents/voltages i/u, input noise ni, output noise
%   nu, exact current i0 and voltage u0 = R0*i0, based on N experiments, 
%   standard deviations on i0, ni, nu sigma_i0, sigma_ni, 
%   sigma_nu and resistor value R0.
    % define input current
    i0 = randn(N, 1);

    % define noise on the input current
    ni = randn(N, 1);

    % scale input signals
    i0 = sigma_i0/sqrt(var(i0))*i0;
    ni = sigma_ni/sqrt(var(ni))*ni;

    % And define the input
    i = i0 + ni;

    % define output noise and voltage
    nu = randn(N, 1);
    u0 = R0 * i0;
    nu = sigma_nu*nu;
    u = u0 + nu;                                   
end