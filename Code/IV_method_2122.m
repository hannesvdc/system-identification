%% Define global data
N = 5000;
fgen = 0.1;
fnoise = 0.6;
sigma_ni = 0.1;
sigma_i0 = 0.1;
sigma_nu = 1;
R0 = 1000;
nb_experiments = 1000;

R_LS = zeros(nb_experiments, 1);
R_IV = zeros(nb_experiments, 3);
s = [1, 2, 5];

%% Compute LS-estimator and IV-estimator for all experiments
for k = 1:1:nb_experiments
    % generate data
    [i, u, ~, ~, ~, ~] = data_generator(N, fgen, ... 
        fnoise, sigma_i0, sigma_ni, sigma_nu, R0);
    for shift_index = 1:3
        R_IV(k, shift_index) = IV_estimator(i, u, s(shift_index));
    end
    R_LS(k) = LS_estimator(i, u);
end

%% Plot histograms and PDF's
% Plot histograms for all estimators and frequencies, if asked
show_histograms = false;
if show_histograms
    figure(1);
    for shift_index = 1:3
        figure(1);
        histogram(R_LS);
        title('LS estimator');
        xlabel('R')
        figure(2);
        subplot(1,3,shift_index);
        histogram(R_IV(:,shift_index));
        title([' IV estimator, shift = ', num2str(s(shift_index))]);
        xlabel('R')       
    end
end

% Plot probability density functions for the estimators.
figure(3);
x = 0:0.01:1500;
estimators = [R_LS, R_IV(:,1), R_IV(:,2), R_IV(:,3)];
for i = 1:size(estimators,2)
    pdf_est = pdf(fitdist(estimators(:,i), 'normal'), x);
    plot(x, pdf_est);
    hold on;
end
title('Probability Density functions');
axis([0 1500 0 0.05]);
ylabel('PDF(R)');
xlabel('R');
legend('LS', 'IV, shift = 1', 'IV, shift = 2', 'IV, shift = 5');

%% Plot the autocorrelation
figure(4);
% generate data
[i, u, ni, nu, i0, u0] = data_generator(N, fgen, ... 
        fnoise, sigma_i0, sigma_ni, sigma_nu, R0);
autocorri0 = xcorr(i0, 10, 'biased');
autocorrni = xcorr(ni, 10, 'biased');
autocorri0 = autocorri0(11:end);
autocorrni = autocorrni(11:end);
x = 0:10;

for shift_index = 1:3
    subplot(1,3,shift_index);
    pai = plot(x, autocorri0, 'x-');    
    hold on;
    plot(s(shift_index),autocorri0(s(shift_index)+1),'k*');  
    disp(['autocorrelation i_0 with shift ', num2str(s(shift_index)),...
            ' : ', num2str(autocorri0(s(shift_index)+1))])
    pani = plot(x, autocorrni, 'x-');    
    disp(['autocorrelation n_i with shift ', num2str(s(shift_index)),...
            ' : ', num2str(autocorrni(s(shift_index)+1))])    
    plot(s(shift_index),autocorrni(s(shift_index)+1),'k*');        
    legend([pai, pani], 'i_0', 'n_i');
    title('Autocorrelation of i_0 and n_i');   
    xlabel('Lag')
    ylabel('Autocorrelation')
    hold off;
end


%% Plot the frequency responses
figure(5);

% define generator filter
[bgen, agen] = butter(1, fgen);

% define noise filter
[bnoise, anoise] = butter(2, fnoise);

% define frequency response function
[h, w] = freqz(bgen, agen, 'whole');
h = 20*log10(abs(h));
h = h(1:end/2);
w = w(1:end/2);

plot(w/(2*pi), h);
hold on;
[h, w] = freqz(bnoise, anoise, 'whole');
h = 20*log10(abs(h));
h = h(1:end/2);
w = w(1:end/2);
plot(w/(2*pi), h);
title(['Filter characteristics of i_0 and n_i, f_{noise} = ', ...
    num2str(fnoise)])
axis([0 0.5 -50 5])
legend('Filter i_0', 'Filter n_i');
xlabel('f/f_s')
ylabel('Filter |dB|')
