% Generate common data
close all
clear
[b,a] = cheby1(3,0.5,[2*0.15 2*0.3]);
Nest = 1000;
Nval = 10000;
% Plot frequency response: indeed pass band filter
figure(1)
[h, w] = freqz(b, a, 'whole');
h = mag2db(abs(h));
h = h(1:end/2);
w = w(1:end/2);
plot(w/pi, h);
title('Frequency response filter')
xlabel('f/f_s')
ylabel('Magnitude')

%% experiment 1
% Generate the data
u0 = randn(Nest,1);
u0val = randn(Nval, 1);
y0 = filter(b,a,u0);
y0val = filter(b, a, u0val);
sigma_ny = 0.05;
ny = sigma_ny*randn(Nest, 1);
nyval = sigma_ny*randn(Nval, 1);
y = y0 + ny;
yval = y0val + nyval;
Imax = 100;

Vest = zeros(Imax, 1);
Vval = zeros(Imax, 1);
VAIC = zeros(Imax, 1);
V0 = zeros(Imax, 1);

for I = 1:1:Imax
    % Solve the least squares estimator.
    r = zeros(1, I);
    r(1) = u0(1);
    K = toeplitz(u0, r);
    g = K \ y;      % g(1) = g0, g(2) = g1 and so on

    if I == 20
        figure(5);
        d = mag2db(abs(fft(g)));
        d = d(1:(length(d)/2));
        plot(linspace(0,1,length(d)), d);
    end
    % Define a digital system with the transfer function
    % check if the solution is accurate
    system = filter(g, 1, u0);
    systemval = filter(g, 1, u0val);
    Vest(I) = norm(system - y)^2 / (Nest*sigma_ny^2);
    Vval(I) = norm(systemval - yval)^2 / (Nval*sigma_ny^2);
    VAIC(I) = Vest(I) * ( 1 + 2*I/Nest);
    V0(I) = sqrt(norm(systemval - y0val)^2 / (Nval*sigma_ny^2));
end

% Plot the results for the validation data. Optimal value is I = 20.
figure(2);
plot(Vval);
hold on;
plot(Vest);
hold on;
plot(VAIC);

title('Cost functions V_{est}, V_{val} and V_{AIC}')
legend('V_{val}', 'V_{est}', 'V_{AIC}');
axis([0 100 0.7 1.2])
xlabel('Order')
ylabel('Cost')

figure(3);
plot(V0);
title('V_0 for noise variance 0.05');
legend('V_0');
xlabel('Model order');
ylabel('Cost function');
