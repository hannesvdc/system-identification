function R_LS = LS_estimator(i, u)
% LS_ESTIMATOR Compute the least squares estimator for the resistor 
% problem.
%   R_LS = LS_ESTIMATOR(i, u) computes the
%   LS-estimator R_LS, based on input i and output u.
    R_LS = (u'*i)/(i'*i);
end