%% Define global data
N = 5000;
sigma_ni = 0.001;
sigma_i0 = 0.01;
sigma_nu = 1;
R0 = 1000;
nb_experiments = 1000;
s = 1;

R_LS = zeros(nb_experiments, 1);
R_EIV = zeros(nb_experiments, 1);

%% Compute LS-estimator and IV-estimator for all experiments
for k = 1:1:nb_experiments
    % generate data
    [i, u, ni, nu, i0, u0] = data_generator_EIV(N, sigma_i0, sigma_ni, sigma_nu, R0);

    % lsq estimator
    R_LS(k) = LS_estimator(i, u);
    
    % EIV estimator
    R_EIV(k) = EIV_estimator(i, u, sigma_ni, sigma_nu);
end

%% Plot histograms
% Plot histograms for all estimators, if asked
show_histograms = true;
if show_histograms
    figure(1);
    subplot(1,2,1);
    histogram(R_LS(:));
    title('LS');
    xlabel('R')
    subplot(1,2,2);
    histogram(R_EIV(:));
    title('EIV');
    xlabel('R');
end

% Plot probability density functions for the estimators.
figure(2);
x = 0:0.01:1500;
estimators = [R_LS(:,1), R_EIV(:,1)];
for i = 1:size(estimators,2)
    pdf_est = pdf(fitdist(estimators(:,i), 'normal'), x);
    plot(x, pdf_est);
    hold on;
end
title('Probability Density functions');
axis([980 1020 0 0.3]);
ylabel('PDF(R)');
xlabel('R');
legend('LS', 'EIV');
