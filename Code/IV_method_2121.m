%% Define global data
clear all
N = 5000;
fgen = 0.1;
fnoise = [0.999, 0.95, 0.6];
sigma_ni = 0.1;
sigma_i0 = 0.1;
sigma_nu = 1;
R0 = 1000;
nb_experiments = 1000;

R_LS = zeros(nb_experiments, 3);
R_IV = zeros(nb_experiments, 3);
s = 1;

%% Compute LS-estimator and IV-estimator for all experiments
for k = 1:1:nb_experiments
    for noise_index = 1:3
        % generate data
        [i, u, ni, nu, i0, u0] = data_generator(N, fgen, ... 
            fnoise(noise_index), sigma_i0, sigma_ni, sigma_nu, R0);

        % lsq estimator
        R_LS(k, noise_index) = LS_estimator(i, u);
        % IV estimator
        R_IV(k, noise_index) = IV_estimator(i, u, s);
    end
end

%% Plot histograms and PDF's
% Plot histograms for all estimators and frequencies, if asked
show_histograms = false;
if show_histograms
    figure(1);
    for noise_index = 1:3
        figure(1);
        subplot(1,3,noise_index);
        histogram(R_LS(:,noise_index));
        title(['f_{noise} = ', num2str(fnoise(noise_index)), ' LS']);
        xlabel('R')
        figure(2);
        subplot(1,3,noise_index);
        histogram(R_IV(:,noise_index));
        title(['f_{noise} = ', num2str(fnoise(noise_index)), ' IV']);
        xlabel('R')       
    end
end

% Plot probability density functions for the estimators.
figure(3);
x = 0:0.01:1500;
estimators = [R_LS(:,1), R_LS(:, 2), R_IV(:,1), R_IV(:,2), R_IV(:,3)];
for i = 1:size(estimators,2)
    pdf_est = pdf(fitdist(estimators(:,i), 'normal'), x);
    plot(x, pdf_est);
    hold on;
end
title('Probability Density functions');
axis([0 1500 0 0.1]);
ylabel('PDF(R)');
xlabel('R');
legend('LS','LS2', 'IV, f_{noise} = 0.999', 'IV, f_{noise} = 0.95', 'IV, f_{noise} = 0.6');

%% Plot the autocorrelation
figure(4);
for noise_index = 1:3
    % generate data
    [i, u, ni, nu, i0, u0] = data_generator(N, fgen, ... 
            fnoise(noise_index), sigma_i0, sigma_ni, sigma_nu, R0);
        
    autocorri0 = xcorr(i0, 10, 'biased');
    autocorrni = xcorr(ni, 10, 'biased');
    autocorri0 = autocorri0(11:end);
    autocorrni = autocorrni(11:end);
    x = 0:10;

    subplot(1,3,noise_index);
    pai = plot(x, autocorri0, 'x-');
    hold on;
    plot(s,autocorri0(s+1),'k*');  
    pani = plot(x, autocorrni, 'x-');
    plot(s,autocorrni(s+1),'k*');        
    legend([pai, pani], 'i_0', 'n_i');
    title('Autocorrelation of i_0 and n_i');
    xlabel('Lag')
    ylabel('Autocorrelation')
    hold off;
end

%% Plot the frequency responses
figure(5);
for noise_index = 1:3
    subplot(1,3,noise_index);
    % define generator filter
    [bgen, agen] = butter(1, fgen);

    % define noise filter
    [bnoise, anoise] = butter(2, fnoise(noise_index));

    [h, w] = freqz(bgen, agen, 'whole');
    h = 20*log10(abs(h));
    h = h(1:end/2);
    w = w(1:end/2);
    plot(w/(2*pi), h);
    hold on;
    [h, w] = freqz(bnoise, anoise, 'whole');
    h = 20*log10(abs(h));
    h = h(1:end/2);
    w = w(1:end/2);
    plot(w/(2*pi), h);
    title(['Filter characteristics of i_0 and n_i, f_{noise} = ', ...
        num2str(fnoise(noise_index))])
    axis([0 0.5 -50 5])
    legend('Filter i_0', 'Filter n_i');
    xlabel('f/f_s')
    ylabel('Filter |dB|')
end