function [i, u, ni, nu, i0, u0] = data_generator(N, fgen, fnoise, ...
                                    sigma_i0, sigma_ni, sigma_nu, R0)
% DATA_GENERATOR Generates data as described in the experiments.
%   [i, u, ni, nu, i0, u0] = DATA_GENERATOR(N, fgen, fnoise, sigma_i0, 
%                                               sigma_ni, sigma_nu, R0) 
%   generates measured currents/voltages i/u, input noise ni, output noise
%   nu, exact current i0 and voltage u0 = R0*i0, based on N experiments, 
%   cut-off frequency fgen for the input, cut-off frequency fnoise for the
%   input noise, standard deviations on i0, ni, nu sigma_i0, sigma_ni, 
%   sigma_nu and resistor value R0.

    % define input current
    e1 = randn(N, 1);
    [bgen, agen] = butter(1, fgen);
    i0 = filter(bgen, agen, e1);

    % define noise on the input current
    e2 = randn(N, 1);
    [bnoise, anoise] = butter(2, fnoise);
    ni = filter(bnoise, anoise, e2);

    % scale input signals
    i0 = sigma_i0/sqrt(var(i0))*i0;
    ni = sigma_ni/sqrt(var(ni))*ni;

    % And define the input
    i = i0 + ni;

    % define output noise and voltage
    nu = randn(N, 1);
    u0 = R0 * i0;
    nu = sigma_nu*nu;
    u = u0 + nu;    
end