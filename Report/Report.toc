\select@language {english}
\contentsline {section}{\numberline {1}Exercise session 1: Noise on Input and Output}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Experiment 1}{1}{subsection.1.1}
\contentsline {paragraph}{Bias of Least Squares Estimator}{1}{section*.2}
\contentsline {paragraph}{Bias of Instrument Variables Estimator}{2}{section*.4}
\contentsline {subsection}{\numberline {1.2}Observations and analysis experiment 2}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Experiment 3}{5}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Bias of the Errors-In-Variables method}{5}{subsubsection.1.3.1}
\contentsline {section}{\numberline {2}Exercise session 2: Model selection using AIC}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Model Selection using the AIC criterion}{7}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Behaviour cost function based on estimation data}{7}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Behaviour cost funtion based on validation data}{7}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Behaviour cost function based on the Akaike Information Criterion}{9}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Comparison of the cost functions for two different noise variances}{9}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}Cost function based on the undisturbed output}{9}{subsubsection.2.1.5}
