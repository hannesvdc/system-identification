\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}

\date{Academic year 2016-2017}
\address{
	Master Mathematical Engineering \\
	System Identification and Modeling \\
	Prof. P. Dreesen}
\title{Part I, Task I: Noise on Input and Output}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
\maketitle	

\tableofcontents

\section{Exercise session 1: Noise on Input and Output}
In this exercise session, we will consider three methods of estimating the value of a resistance where the inputs and outputs are disturbed by noise. We will analyze and explain the numerical results.

\subsection{Experiment 1}
In this section, we will carry out the experiments proposed in the assignment concerning the LS and IV estimator with shift $s=1$ for three difference cut off frequencies $f\textsubscript{noise}$. First we will deduce som theoretical expressions related to the bias of the estimators, and then we will perform the numerical experiments themselves. The probability density functions of the LS and IV estimator are shown in figure \ref{fig:pdf_estimator}, the autocorrelation of IV for the different noise frequencies in Figure \ref{fig:autocorrelation_fig1} and the noise response in Figure \ref{fig:exp1freq}.

\paragraph{Bias of Least Squares Estimator}

We deduce an expression of the bias of the LS-estimator as function of $\sigma_{n_i}^2 \text{ and } \sigma_{i_0}^2$:
\begin{equation*}
\begin{split}
\lim_{N \to +\infty} \hat{R}_{LS}  & = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}u(k)i(k)}{\sum_{k = 1}^{N}i(k)^2} = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}(u_0(k)+n_u(k))(i_0(k)+n_i(k))}{\sum_{k = 1}^{N}(i_0(k)+n_i(k))^2} \\ 
& = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}(u_0(k)i_0(k) + \cancel{u_0(k)n_i(k)}+\cancel{n_u(k)i_0(k)}+\cancel{n_u(k)n_i(k)}}{\sum_{k = 1}^{N}(i_0(k)^2+\cancel{2i_0(k)n_i(k)}+n_i(k)^2)} \\
& \stackrel{u_0 = R_0i_0}{=} \lim_{N \to +\infty} \frac{R_0\sum_{k = 1}^{N}i_0(k)^2}{\sum_{k = 1}^{N}(i_0(k)^2+n_i(k)^2)} =  \lim_{N \to +\infty} \frac{R_0\sigma_{i_0}^2}{\sigma_{i_0}^2 + \sigma_{n_i}^2}\\
& = \frac{R_0}{1+\frac{\sigma_{n_i}^2}{\sigma_{i_0}^2}}
\end{split}
\end{equation*}

The crossed-out terms in the equations are zero, because both measurements $u_0$ and $i_0$ are disturbed by mutually independent Gaussian noise $n_i$ and $n_u$ and we assume that the noise is uncorrelated with the input and output values.

Based on the expression for $\hat{R}_{LS}$, we see that the noise on the input ($\sigma_{n_i}^2$) causes the bias of the estimator. If we compute the bias, based on the found expression and with the data of experiment 1, we obtain 
\[
\lim_{N \to +\infty} \hat{R}_{LS} = \frac{1000}{1+\frac{0.1^2}{0.1^2}} = \frac{1000}{2} = 500.
\]
This value is confirmed in Figure \ref{fig:pdf_estimator}: we see that the peak of the distribution for $\hat{R}_{LS}$ lies around resistor value $500 \Omega$. Also notice that this is independent from the noise cut-off frequency $f\textsubscript{noise}$.

\begin{figure}
	\includegraphics[width=\linewidth]{pdf_estimators}
	\caption{The probability density functions of $\hat{R}_{LS}$ and $\hat{R}_{IV}$ for $f_{noise} = \{0.999, 0.95, 0.6\}$, with fixed shift parameter $s = 1$.}
	\label{fig:pdf_estimator}
\end{figure}

\paragraph{Bias of Instrument Variables Estimator}

We deduce an expression of the bias of the IV-estimator as function of the autocorrelations $R_{n_in_i}(s) \text{ and } R_{i_0i_0}(s)$:
\begin{equation*}
\begin{split}
\lim_{N \to +\infty} \hat{R}_{IV}  & = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}u(k)i(k+s)}{\sum_{k = 1}^{N}i(k)i(k+s)} = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}(u_0(k)+n_u(k))(i_0(k+s)+n_i(k+s))}{\sum_{k = 1}^{N}((i_0(k)+n_i(k))(i_0(k+s)+n_i(k+s))} \\ 
& = \lim_{N \to +\infty} \frac{\sum_{k = 1}^{N}(u_0(k)i_0(k+s) + \cancel{u_0(k)n_i(k+s)}+\cancel{n_u(k)i_0(k+s)}+\cancel{n_u(k)n_i(k+s)}}{\sum_{k = 1}^{N}(i_0(k)i_0(k+s)+\cancel{i_0(k)n_i(k+s)}+\cancel{n_i(k)i_0(k+s)}+n_i(k)n_i(k+s))} \\ \label{eqq}
& \stackrel{u_0 = R_0i_0}{=} \lim_{N \to +\infty} \frac{R_0\sum_{k = 1}^{N}i_0(k)i_0(k+s)}{\sum_{k = 1}^{N}(i_0(k)i_0(k+s)+n_i(k)n_i(k+s))} =  \lim_{N \to +\infty} \frac{R_0R_{i_0i_0}(s)}{R_{i_0i_0}(s) + R_{n_in_i}(s)}\\
& = \frac{R_0}{1+\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}}
\end{split}
\end{equation*}

If we compute the bias, based on the found expression and with the data of experiment 1, we obtain for $f_{noise} = 0.999$
\[
\lim_{N \to +\infty} \hat{R}_{IV} \approx \frac{1000}{1+\frac{0}{\num{9e-3}}} = 1000.
\]
Where we obtain $R_{n_in_i}(s)$ and $R_{i_0i_0}(s)$ from Figure \ref{fig:autocorrelation_fig1}, evaluated at $s = 1$. In the same way, we find for $f_{noise} = 0.95$ and $f_{noise} = 0.6: \lim_{N \to +\infty} \hat{R}_{IV} \approx 900$ and $ \lim_{N \to +\infty} \hat{R}_{IV} \approx 643$. These values correspond with the values of Figure \ref{fig:pdf_estimator}.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{autocorrelation_fig1}
	\caption{The autocorrelation of $i_0$ and $n_i$ for different noise filters, with $f_{noise} = \{0.999, 0.95, 0.6\}$.}
	\label{fig:autocorrelation_fig1}
\end{figure}

If on the other hand the input noise were to be unfiltered white noise (cfr. the case of $f_{noise} = 0.999$), then the autocorrelation is a scaled delta function in discrete time, which means that $R_{n_in_i}(s) = 0 $ for $s \neq 0$. Then, according to the formula above, there should be no bias.

We also clearly see that the distribution of the output noise has no impact at all on the previous formula. As long as the output noise is uncorrelated with the inputs, the formula holds. As a consequence, it is possible to filter the output noise and keep the same value for the estimator, if we keep the filtered signal independent from the input noise and current.

Another interesting figure are the frequency responses of the input noise and the input current after applying the Butterworth filter. These are shown in Figure \ref{fig:exp1freq}.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{frfexp1}
	\caption{The frequency responses of $i_0$ and $n_i$ for different noise filters, with $f_{noise} = \{0.999, 0.95, 0.6\}$.}
	\label{fig:exp1freq}
\end{figure}
From the figures, we conclude that the noise has fewer high frequent components as $f_{\text{noise}}$ decreases. This means that the noise behaves more smooth and the autocorrelation will be higher when we increase the shifts. A consequence is that the bias for IV will be larger in this case, as can be seen in Figure \ref{fig:pdf_estimator}. This is conformal to the explanation in the book that the bandwidth of the input signal should be significantly smaller than the bandwidth of the noise.

\subsection{Observations and analysis experiment 2}
Let us now fix the noise frequency $f\textsubscript{noise} = 0.6$ and consider three values for the shift parameter $s = 1,2,5$. Figure \ref{fig:pdf_estimators_2122} shows that, as the shift variable $s$ increases (independently of the filters), the bias of the IV-estimator becomes smaller. 

\begin{figure}
	\includegraphics[width=\linewidth]{pdf_estimators_2122}
	\caption{The probability density functions of $\hat{R}_{LS}$ and $\hat{R}_{IV}$ for shift parameter $s = \{1, 2,5\}$, with fixed noise frequency $f_{noise} = 0.6$.}
	\label{fig:pdf_estimators_2122}
\end{figure}

We can explain this behavior, considering the formula for the bias of $\hat{R}_{IV}$: 
\[
\lim_{N \to +\infty} \hat{R}_{IV} = \frac{R_0}{1+\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}}.
\]

The exact values for the autocorrelations in function of shift parameter $s$ are shown in Table \ref{tab:autocorrelations_shift}.

\begin{table}
	\centering
	\begin{tabular}{| c | c | c | c | c |}
		\hline
		$s$ & $R_{i_0i_0}(s)$ & $R_{n_in_i}(s)$ & $\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}$ & $\hat{R}_{IV}$\\
		\hline
		1 & \num{8.6e-3} & \num{4.8e-3} & 0.56 &  642.8 \\
		2 & \num{6.2e-3} & \num{-1.1e-3} & -0.19 & 1239.9 \\
		5 & \num{2.2e-3} & \num{-0.02e-3} & -0.08 & 1088.2 \\
		\hline
	\end{tabular}
	\caption{The autocorrelation for $i_0$ and $n_i$, as well as the resulting IV-estimator $\hat{R}_{IV}$.}
	\label{tab:autocorrelations_shift}
\end{table}

Figure \ref{fig:autocorrelation_2122} supports graphically the results of Table \ref{tab:autocorrelations_shift}. What we conclude, is that, the larger shift parameter $s$ becomes, the smaller the ratio $\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}$ becomes, so the less biased the IV-estimator $\hat{R}_{IV}$ will be.

\begin{figure}
	\includegraphics[width=\linewidth]{autocorrelation_2122}
	\caption{The autocorrelation of $i_0$ and $n_i$, with the three different shift parameters ($s = 1, 2, 5$) marked. (Note: the autocorrelation remains constant)}
	\label{fig:autocorrelation_2122}
\end{figure}

Figure \ref{fig:pdf_estimators_2122} shows that shifts $s = 2$ and $s = 5$ give an overestimate, while shift parameter 2 gives an underestimate. Based on the autocorrelation of the noise (Figure \ref{fig:autocorrelation_2122} and Table \ref{tab:autocorrelations_shift}), we see that for the overestimates, the autocorrelation of the noise ($R_{n_in_i}(s)$) is negative (while $R_{i_0i_0}(s)$ is positive). Dependent on the input and noise filter, the autocorrelation will show such a behaviour. This behaviour results in a negative ratio $\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}$, which results in a denumerator $ < 1$, and thus an overestimate. The reasoning for the underestimate is similar. We conclude that the sign of the bias depends on the sign of the autocorrelation of the input noise (while the autocorrelation of the input is positive).

Based on Figure \ref{fig:autocorrelation_2122}, we also notice that the dispersion (variance) of the IV-estimator becomes larger if $s$ increases. If we consider the theory, we obtain the following expression for the covariance matrix of the IV-estimator:
\[
\text{Cov}\{\hat{\theta}_{IV}(N)\} \approx \sigma^2R_{GK}^{-1}R_{GG}R_{GK}^{-1}.
\]
If shift parameter $s$ increases, then $G$ (shifted input) becomes more uncorrelated with $K$ (unshifted input) (see Figure \ref{fig:autocorrelation_2122}: the autocorrelation $R_{i_0i_0}(s)$ decreases with $s$). This results in a `smaller' $R_{GK}$ and thus a larger $R_{GK}^{-1}$. The results is that the covariance becomes larger is $s$ increases.

Again, the frequency responses of the input noise and input current is plotted in Figure \ref{fig:exp2freq}. We see that the bandwidth of the input signal is not significantly smaller than the noise bandwidth, which implies that we need a larger shift of $s=5$ to get a good estimate, as seen in Figure \ref{fig:pdf_estimators_2122}.
As a general rule of thumb, the IV method works well if the bandwidth of the generator is much smaller than the bandwidth of the noise.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{frfexp2}
	\caption{The frequency responses of $i_0$ and $n_i$ for the second experiment and noise filter with $f_{noise} = 0.6$.}
	\label{fig:exp2freq}
\end{figure}

\subsection{Experiment 3}
Lets now turn to a third identification technique: the Errors-In-Variables method. Figure \ref{fig:pdfeiv} shows the probability density function of EIV.
\subsubsection{Bias of the Errors-In-Variables method}

Figure \ref{fig:pdfeiv} shows the PDF of the LS and EIV estimators for the resistor problem. The values of the variances are $\sigma_{n_i} = 0.001$ and $\sigma_{i_0} = 0.01$. From this we deduce that there is a bias in the LS estimator of $\frac{R_0}{1+\frac{0.001^2}{0.01^2}} = \frac{1000}{1.01}\approx990$. The mean of the LS estimator is indeed centered around 990.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{eivpdf}
	\caption{The PDF for the LS and EIV estimator for the resistor problem.}
	\label{fig:pdfeiv}
\end{figure}
In the same figure, we experimentally deduce that the EIV estimator is unbiased for this problem. The experimental mean we encountered was 999.9425.

Suppose now that we apply the IV method to this problem. We ran the experiments and saw that distribution of the IV estimator is almost constant. This implies that the estimator is useless for this problem.
One explanation for this phenomenon is the following. In the formula for the bias of IV, we encounter the term $\frac{R_{n_in_i}(s)}{R_{i_0i_0}(s)}$. For white inputs and noise, both autocorrelations are 0 for $s \neq 0$. This implies that the limit of the IV estimator can be anything.
Another more fundamental explanation can be found in the formula for the covariance matrix of $\hat{R}_{IV}$. If $C_{n_y} = \sigma^2 I_N$, then this is given by 
\[
\text{Cov}(\hat{R}_{IV}) = \sigma^2R^{-1}_{GK} R_{GG} R^{-T}_{GK}
\]
with $R_{GK} = \frac{G^T G}{N}$. The problem here is that $G$ is calculated using values at time $k-1$, which implies that $G$ is almost uncorrelated with $K$ due to the behavior of white noise. As a consequence, the covariance matrix tends to get very large, which explains the almost flat PDF we saw while running the IV method.

\section{Exercise session 2: Model selection using AIC}
This part of the report covers the second exercise session about model selection and the AIC criterion.

\subsection{Model Selection using the AIC criterion}

Suppose we approximate the given IIR system with a FIR system of order $I$ where we increase $I$ iteratively. This implies that the system was initially at rest, otherwise transient behaviour would occur that is damped out of the discrete system after $I$ steps because this is the length of the impulse reponse.
 The system is fitted using a standard least squares approach, based on \emph{estimation data}. The input data consists of a normally distributed input sequence of length $N_{est}$. The output was calculated by passing the input through a Chebyshev filter and adding Gaussian white noise to it. Figure \ref{fig:cost_functions} shows the cost functions $V$\textsubscript{est}, based on the estimation data, $V$\textsubscript{val}, based on the validation data and $V$\textsubscript{AIC}, based on the Akaike information criterion, in function of the model order $I$, for two different noise variances.


\begin{figure}
\begin{subfigure}[b]{\textwidth}
\includegraphics[width=\textwidth]{cost_functions_05}
\caption{Cost functions on the estimation and validation data and the AIC cost function for $\sigma_{n_y}=0.5$}
\label{fig:cost_functions_0.5}
\end{subfigure}

\begin{subfigure}[b]{\textwidth}

\includegraphics[width=\textwidth]{cost_functions_005}

\caption{Cost functions on the estimation and validation data and the AIC cost function for $\sigma_{n_y}=0.05$}

\label{fig:cost_function_005}

\end{subfigure}
\caption{Cost functions on the estimation and validation data and the AIC cost function on the for $\sigma_{n_y} = 0.5, 0.05$}
\label{fig:cost_functions}
\end{figure}

\subsubsection{Behaviour cost function based on estimation data}


In Figure \ref{fig:cost_functions}, we clearly see that the total error (cost) on the estimation data decreases monotonically. This is a logical result because we choose the coefficients in such way that the cost function $V$\textsubscript{est} was minimized. Hence, by increasing $I$ we get more degrees of freedom meaning that we can more minimize the cost based on the estimation data. Intuitively, we of course feel that an unbounded increase will not yield a good fit because from a certain order, we will be modelling the output noise, and not the system itself. This is were the validation data enters the scene.
As a primer, Figure \ref{fig:frfs} shows the frequency responses of our modelled system of order $I=20$, and the exact Bode plot of the Chebychev filter. Twenty will turn out to be close to the optimal order of the system. We notice that the estimated system follows the exact system very accurately where the exact system is active (band pass around 0.5). For low and high frequencies, the noise in the estimation data will take over from the exact system.

\begin{figure}
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\linewidth]{frfcheb}
		\caption{The Bode plot of the exact Chebychev filter.}
	\end{subfigure}
	\begin{subfigure}[b]{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\linewidth]{frfI20}
		\caption{The estimated Bode plot using order 20.}
	\end{subfigure}
	\label{fig:frfs}
\end{figure}

\subsubsection{Behaviour cost funtion based on validation data}

We can test the last claim of the previous section by generating so-called \emph{validation data} in the same way as the estimation data and then comparing the output of the identified system with the disturbed output of the Chebyshev filter. 


On Figure \ref{fig:cost_functions_0.5}, we see that the error on the validation data reaches a minimum around $I_{\text{opt}} = 20$ and that the error increases afterwards. For low orders, $I > I_{\text{opt}}$, we can explain the decrease of $V$\textsubscript{val} in function of increasing model order as follows: the model order is that low that any extra degree of freedom models a lot of extra relevant information, because we don't have many parameters in our model. In that case, you are only modeling relevant information. The identified system does not `catch' enough of the dynamic behavior of the system. Then we reach an optimal order, for which the error on the random validation data set is minimal. If we then increase the model order further, the cost on the validation data set increases. This is because for higher orders, you are only modeling noise because the real system decreases exponentially and the noise is the dominant term in the impulse response. The extra degrees of freedom are mainly used to fit the estimation data as good as possible, without an underlying physical meaning anymore. All relevant information is captured by the first parameters. You get the phenomenon of overfitting: the model becomes too flexible, too sensitive to noise. You are modeling the randomness of the measurements, which is a contradictio in terminis. 

\subsubsection{Behaviour cost function based on the Akaike Information Criterion}


The goal of the Akaike Information Criterion (AIC) is to include the complexity of the model in the cost function by penalizing higher order models. More concrete, the new cost function is given by 
\[V_{\text{AIC}} = V_{\text{est}}(1+2\frac{\text{dim}(\theta)}{N}),\] 
where $V_{\text{est}}$ is the standard least squares cost function, $\text{dim}(\theta)$ is the number of model parameter ($I$ in this example), and $N$ is the number of inputs we apply.


The goal of AIC is to eliminate the use of validation data and mimic the least squares cost function on the validation data. Our experiments confirm the same behaviour and more or less the same optimal order, for different noise variances.


\subsubsection{Comparison of the cost functions for two different noise variances}


If we compare the situation where the noise standard deviation is $0.5$ with a standard deviation of $0.05$ in Figures \ref{fig:cost_functions_0.5} and \ref{fig:cost_function_005}, we see a few big differences. Most notably is that the optimal order increases when the the variance decreases. An intuitive explanation for this behavior is the following. When the variance is low, we have a greater confidence in our data such that we can employ a model of higher order that explains more of the dynamic behavior of the system. If the noise were bigger, then higher order models would try to explain the random noise on the output, which is not explainable. 

You can also derive this if you reason about IIR. Because the system is stable, the tail goes to zero. If there is much noise present, the tail will be dominated earlier by the noise (because the signal-to-noise ratio decreases faster), such that you have take less impulse response coefficients into account. Otherwise, you would be modeling noise. So, if the variance on the noise increases, the optimal model order decreases.

As a consequence, if there were no noise present, the optimal order of the identified system would be infinite and the estimated system would be exactly equal to the Chebyshev filter.


\subsubsection{Cost function based on the undisturbed output}


Figure \ref{fig:v_cost} shows the normalized 2-norm of the error of the model output relative to the undisturbed output of the validation set: $V_0 = \frac{1}{N}\sum_{k = 1}^{N}(y_0(k)-\hat{y}(k))^2$, as a function of the model order $I$. Looking at $\sqrt{\frac{V_0}{\sigma_{n_y}^2}}$, rather than at $V$\textsubscript{val}, is more relevant. This is because you only want to model relevant information, not the noise. $V_0$ shows the errors relative to the undisturbed output $y_0$: this is the real behaviour of the system, this is what we want to model. So it is more relevant to test our modeled system against this real output, rather then also taking the noise into account (as in $V$\textsubscript{val}). Notice that in pratical, this undisturbed output is logically not available.

\begin{figure}
\centering
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{v005}
\caption{Normalized cost of the undisturbed output for \\$\sigma_{n_y}=0.5$.}
\label{fig:v005}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
\includegraphics[width=\textwidth]{v0005}
\caption{Normalized cost of the undisturbed output for \\$\sigma_{n_y}=0.05$.}
\label{fig:v0005}
\end{subfigure}
\caption{Normalized cost of the undisturbed output for $\sigma_{n_y}=0.5, 0.05$.}
\label{fig:v_cost}
\end{figure}

On these figures, as well as on Figure \ref{fig:cost_functions}, we see that the function behaves very flat around the minimizer, meaning that the optimal order of the estimated system is not very critical. The order can easily be 5 bigger or less than the optimal order without dramatically increasing the total cost.


What happens when we increase the order of the model relative to the cost $V_0$? If we increase the order, then, as said previously, the estimated system will try to explain the random noise on the estimation data. This is why the cost function $V_0$ will also increase, namely the exact output has different noise values (in this particular case none) than the estimation data. Due to this fact, the cost function $V_0$ will increase again.

\end{document}