%% ARMAX Modelling
% Generate estimation and validation data
u_est = generateInputs('pseudowhite', 3);
y_est = exercise2(u_est);
y_est = preprocess(y_est);
u_val = generateInputs('pseudowhite', 3);
y_val = exercise2(u_val);
y_val = preprocess(y_val);

% Initialisation
na = 10;
nb = 10;
nc = 1:20;
models = cell(1,1,20);
maxim = 0;
bestorder = 0;

% Compute ARMAX models
for i = 1:length(na)
    na_ = na(i);
    for j = 1:length(nb)
        nb_ = nb(j);
        for k = 1:length(nc)
            nc_ = nc(k);
            models{i, j, k} = armax([y_est, u_est], [na_ nb_ nc_ 17]);
            [~, fit,~] = compare([y_val, u_val], models{i, j, k}, 1);
            if fit > maxim
                maxim = fit;
                bestorder = [na_ nb_ nc_ 17];
            end
        end
    end
end

% Build model
modelarmax = armax([y_est, u_est], bestorder);

% Process model
modelarmaxreduced = processModel(modelarmax, ['ARMAX ', num2str(bestorder)], [y_est, u_est], [y_val, u_val]);