%% Experimental bode plot
bodeplot = zeros(500, 1);

for freq = 1:500
    if mod(freq, 100) == 0 
        disp(freq)
    end
    
    % Generate the input and compute the FFT.
    [input_data, u] = generateInputs('sine', 3, freq/1000);
    finput = abs(fft(input_data));
    inputmax = max(finput);
    
    y_tot = zeros(3000, 1);
    amax = 5;
    for a = 1:amax
        % Compute the output and preprocess
        y = exercise2(input_data);
        y = preprocess(y);
        y_tot = y_tot + y;
    end
    y = y_tot/amax;
    f = abs(fft(y));
    outputmax = max(f);

    % Calculate the experimental transfer function by dividing the
    % standard deviations.
    bodeplot(freq) = std(f) / std(finput);

end

figure;
plot(linspace(0, pi, 499), smooth(smooth(mag2db(bodeplot(1:end-1)))));
