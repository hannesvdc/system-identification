This file contains an overview of the code files for the second part of the assignment for System Identification.

generateInputs.m        Generic file that generates numerical data of a given type (e.g. 'sine', 'impulse', 'zero', ..)
preprocess.m            Function that perfoms detrending, peak shaving and a high pass filter to the output of a system.
                        Must always be used to preprocess the numeric output of a system.


estimate_delay.m        Numerically estimate the delay in the system by averaging 2000 impulse responses and using 'cra'
                        as a check.
experimental_bode.m     Compute an experimental Bode plot by applying different sine waves with different frequencies.
hankel_order.m          Experimental script to compute the order of the system by setting up the Hankel matrix and computing the
                        numerical rank of this matrix. Does not yield reliable results.


arxstrucid.m            Script that generates white input data and sets up the interactive arxstruc frame to select the 
                        optimal ARX model.
processModel.m          Generic function that displays the fit percentage of a model of the estimation and validation data, 
                        performs a residual analysis and plots the pzmap, it also shows the Hankel singular values and 
                        asks the user for the order to truncate, then also does a residual analysis and a pzmap of this reduced
                        model and finally shows a Bode plot to compare both.
armax_identify.m        Chooses the best ARMAX model by varying the n_c paramter with fixed n_a and n_b paramters. Then also
                        calls 'processModel' to do a more detailed analysis.
oe_identify.m           Performs OE estimation with the optimal paramters from the report and performs a more detailed analysis.
bj_identify.m           Box-Jenkins identification and more detailed numerical analysis.
sub_identify.m          Subsystem identification function for our model.