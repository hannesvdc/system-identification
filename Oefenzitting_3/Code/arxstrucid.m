%% ARXStruc identify
search_region = struc(1:20, 1:20, 16:18);

% Generate estimation and validation data
u_est = generateInputs('pseudowhite', 3);
y_est = exercise2(u_est);
y_est = preprocess(y_est);
u_val = generateInputs('pseudowhite', 3);
y_val = exercise2(u_val);
y_val = preprocess(y_val);

% apply arxstruc and select the optimal model.
V = arxstruc([y_est, u_est], [y_val, u_val], search_region);
order = selstruc(V);
modelarx = arx([y_est, u_est], order);

% Process model
modelarxreduced = processModel(modelarx, ['ARX ' , num2str(order)], [y_est, u_est], [y_val, u_val]);