%% Subspace modeling
% % Generate estimation and validation data
% u_est = generateInputs('pseudowhite', 3);
% y_est = exercise2(u_est);
% y_est = preprocess(y_est);
% u_val = generateInputs('pseudowhite', 3);
% y_val = exercise2(u_val);
% y_val = preprocess(y_val);

% Create model
nk = 17;
modelsub = n4sid([y_est, u_est], nk);

% Process model
modelsubreduced = processModel(modelsub, 'SUB', [y_est, u_est], [y_val, u_val]);

