function y = preprocess(y)
% PREPROCESS Preprocess given data (detrend, high-pass filter and peak shave).
%   Y = PREPROCESS(y) returns preprocessed data.
    [B,A] = butter(2, 0.04, 'high');
    y = detrend(y);
    stdev = std(y);
    y = pkshave(y, [-stdev stdev],0);
    y = filtfilt(B, A, y);
end