function [data, idobject] = generateInputs(type, amplitude, opt)
% GENERATEINPUTS Generate input data from given type, amplitude and option.
%   [DATA, IDOBJECT] = GENERATEINPUT(type, amplitude, opt) generates input
%   data from given string type (step, sine, white, colored, pseudowhite,
%   impulse or zero), given amplitude (default 1), given option (frequency
%   if sine, color if colored.
    s = 3000;
    
    if ~exist('amplitude', 'var')
        amplitude = 1;
    end
        
    if strcmp(type, 'step')   
        data = amplitude*ones(s, 1);
        
    elseif strcmp(type, 'sine')
        % The frequency is in the opt argument
        if ~exist('opt', 'var')
            opt = 1.0;
        end
        data = amplitude * sin(2*pi*opt*(0:s-1)');
        
    elseif strcmp(type, 'white')
        % The variance is in the amplitude argument        
        data = sqrt(amplitude) * randn(s,1);
        
    elseif strcmp(type, 'colored')
        % opt is the color of the noise
        if ~exist('opt', 'var')
            opt = 'white';
        end
        hcn = dsp.ColoredNoise('Color',opt, 'SamplesPerFrame', s);
        data = amplitude * hcn();    
    elseif strcmp(type, 'pseudowhite')
        data = idinput([s, 1], 'prbs', [0, 1], [-amplitude, amplitude] );    
    elseif strcmp(type, 'impulse')
        % impulse response
        data = zeros(s,1);
        data(1) = amplitude;
    elseif strcmp(type, 'zero')
        % zero input to asses noise
        data = zeros(s, 1);
    end
    
    idobject = iddata([], data, 1);
end