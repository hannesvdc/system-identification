% Determine the order of the system numerically - does not work in
% practice.
p = 8;
q = 2000;
[input_data, u] = generateInputs('impulse', 3);
y = exercise2(input_data);
y = preprocess(y);
r_y = y(p:p+q-1)';
c_y = y(1:p);
Y = hankel(c_y,r_y);
r_u = input_data(p:p+q-1)';
c_u = input_data(1:p);
U = hankel(c_u,r_u);
H = [Y;U];
plot(svd(H),'-o');