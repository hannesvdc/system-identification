%% Get a feeling with the system with zero input
% Generate zero input
[input_data, u] = generateInputs('zero');

% Apply this input
y = exercise2(input_data);

% preprocess the data
y = detrend(y);
stdev = std(y);
y_tot = pkshave(y, [-stdev stdev],1);

% Plot the frequency response of the noise.
figure
plot(abs(fft(y_tot)));

% plot data
figure
plot(y_tot);
title('original data');

% Apply a low pass filter to get the sinusoidal trend
[B,A] = butter(2, 0.027);
y = filtfilt(B,A,y_tot);
figure;
plot(y);

% And subtract the noise from the data.
y_noise = y_tot - y;
figure
plot(y_noise);
title('Signal without low frequency noise');
