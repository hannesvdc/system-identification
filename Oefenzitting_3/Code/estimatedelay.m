%% Impulse responses to get an idea of the delay
K = 2000;
[input_data, u] = generateInputs('impulse', 3);
[B,A] = butter(2, 20/500, 'high');
y_tot = zeros(1000,1);

for k=1:K
    if mod(k, 100) == 0
        disp(k);
    end
    
    % Calculate the output and subtract the mean to get meaningful results.
    y = exercise2(input_data);
    y = y - mean(y);
    
    y_tot = y_tot + y;
end

% Average over all experiments and plot to estimate the delay visually.
y_tot = y_tot/K;
figure;
stem(y_tot);    
title('Delay in system');
