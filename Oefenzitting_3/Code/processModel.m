function modelreduced = processModel(model, modelname, est_data, val_data)
% PROCESSMODEL Compute the estimation and validation fit percentage,
% produce the correlation plots, the pole/zero maps and the Hankel singular
% values of the given model. Reduce the model and compare the bode plots.
%   MODELREDUCED = PROCESSMODEL(model, modelname, est_data, val_data) process the given
%   model with string modelname, based on the estimation data [y_est,
%   u_est] in est_data and the validation data [y_val, u_val] in val_data.

% Plot the fit percentage for the estimation and validation data.
compare(val_data, model, 1);
title(['Validation data set, ', modelname])
figure;
compare(est_data, model, 1);
title(['Estimation data set, ', modelname])

% Also do a residual analysis and plot the pzmap
figure;
resid(val_data, model);
title(['Residue correlation, ', modelname])
figure
pzmap(model);
title(['Pole/Zero map, ', modelname])

% Perform model order reduction
figure
hsvd(model);
title(['Hankel Singular Values, ', modelname])
reduceorder = input('What order do you want to use for order reduction? ');
opt = balredOptions('StateElimMethod','Truncate');
modelreduced = balred(ss(model), reduceorder, opt);

% Plot the fit percentage for the estimation and validation data for the reduced model.
compare(val_data, modelreduced, 1);
title(['Validation data set, ', modelname, ' reduced ', num2str(reduceorder)])
figure;
compare(est_data, modelreduced, 1);
title(['Estimation data set, ',  modelname, ' reduced ', num2str(reduceorder)])

% Do a residual analysis and plot the pzmap of the reduced model
figure;
resid(val_data, modelreduced);
title(['Residue correlation, ', modelname, ' reduced ', num2str(reduceorder)])
figure
pzmap(modelreduced);
title(['Pole/Zero map, ',  modelname, ' reduced ', num2str(reduceorder)])

% and compare both in a bode plot
figure;
bode(model, modelreduced);
title(['Bode plot ',  modelname])
legend(modelname, [modelname, ' reduced ', num2str(reduceorder)])
end