%% BJ modelling
order = [16, 13, 9, 9, 17];

% Generate estimation and validation data
u_est = generateInputs('pseudowhite', 3);
y_est = exercise2(u_est);
y_est = preprocess(y_est);
u_val = generateInputs('pseudowhite', 3);
y_val = exercise2(u_val);
y_val = preprocess(y_val);

% Build the model with specified orders
modelbj = bj([y_est,u_est], order);

% Process model
modelbjreduced = processModel(modelbj, ['BJ ', num2str(order)], [y_est, u_est], [y_val, u_val]);