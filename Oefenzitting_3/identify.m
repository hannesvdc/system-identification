%% ARX identify
[input_data, u] = generateInputs('pseudowhite', 3);
output = exercise2(input_data);
output = preprocess(output);
delay = 17;
models = cell(10,10);

for na = 1:10
    for nb = 1:10
        models{ na, nb} = arx([output, input_data], [na, nb, delay]);
    end
end
model = arx([output, input_data], [5,5, delay]);

%% ARMAX Modelling
na = [10];
nb = [10];
nc = 1:20;
models = cell(1,1,20);
maxim = 0;
bestorder = 0;
for i = 1:length(na)
    na_ = na(i);
    for j = 1:length(nb)
        nb_ = nb(j);
        for k = 1:length(nc)
            nc_ = nc(k);
            models{i, j, k} = armax([y_est, u_est], [na_ nb_ nc_ 17]);
            [~, fit,~] = compare([y_val, u_val], models{i, j, k}, 1);
            fit
            if fit > maxim
                maxim = fit;
                bestorder = [na_ nb_ nc_ 17];
            end
        end
    end
end

%% BJ modelling
na = 5:15;
nb = 5:15;
nc = 1:20;
nf = 1:15;
models = cell(11,11,20,15);
maxim = 0;
bestorder = 0;
for i = 1:length(na)
    na_ = na(i);
    for j = 1:length(nb)
        nb_ = nb(j);
        for k = 1:length(nc)
            nc_ = nc(k);
            for l = 1:length(nf)
                nf_ = nf(l);
                models{i, j, k, l} = bj([y_est, u_est], [na_ nb_ nc_ nf_ 17]);
                [~, fit,~] = compare([y_val, u_val], models{i, j, k, l}, 1);
                if fit > maxim
                    maxim = fit;
                    bestorder = [na_ nb_ nc_ nf_];
                end
                disp(['max: ', num2str(maxim)]);
            end
        end
    end
end