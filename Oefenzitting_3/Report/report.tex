\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[margin=8pt]{subfig}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}

\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9} % for two column documents
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9} % for two column documents
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1} 

\date{Academic year 2016-2017}
\address{
	Master Mathematical Engineering \\
	System Identification and Modeling \\
	Prof. Dr. Ir. Bart De Moor, Dr. Ir. Philippe Dreesen}
\title{A Real-life Identification Problem}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
\maketitle
\section{Introduction}
In this report, we will use a few different system identification approaches to identify an unknown system. This system is
prone to severe noise and can have nonlinear effects due to the modeling of mechanical actuators, ... . We will first try
to get a feeling with the system by analyzing the behaviour of different inputs, whereafter we will move on to actual methods of 
identifying the system.


\section{Feeling the system and Preprocessing}
In this section, we will apply different inputs to the unknown system in order to analyze the behaviour. First we will apply 
a zero input to see the behaviour of the noise, to get an estimate of the DC offset, of the trend in the signal, \dots . Then we will apply an impulse to estimate the delay in the system and finally, we will
apply sine functions with different frequencies to compute an approximate bode plot. From all the knowledge gathered, we can finally construct an appropriate preprocesser for our signal to identify our system from collected data.


\subsection{Zero input}
Applying a zero input to the system can give valuable information about the dynamics of the system when only noise is present. This can easily be seen, 
if the system has the following model:
\[
y(k) = G(q)u(k) + H(q)e(k),
\]
where $e(e)$ is the noise term, then only $H(q)e(k)$ remains and this is the system dynamics of the noise.

When we applied this zero input to the system, we got the graph in Figure \ref{fig:zeroinputoriginal}. On this graph we can already see a couple
of characteristics of the sytem. First of all, there seems to be a DC component of $\approx 17.5$. This is a constant offset in the system and we want 
to avoid this. Secondly, there are also peaks in the signal. This may be due to measurement of the output signal. This data is corrupted and
we want to get rid of this. Peakshaving is a useful tool here. Thirdly, we can also see a sinusoidal trend in the system with a low frequency.
This indicates that there is a component of the noise that has low frequencies. For further use of the data, we also need to discard these low 
frequencies by use of a highpass filter. The low frequency trend in shown in Figure \ref{fig:zeroinputtrend}.
More specifically, we preprocessed the output by undertaking the following steps.
\begin{itemize}
	\item Detrend the data using MATLAB `detrend'. This removes the DC component from the output signal, as well as a linear trend.
	\item Peak shaving: Shave the peaks that are outside the range [-standard deviation, standard deviation] using the function that was given in the assignment. We chose this particular range because it adapts to different input signals.
	\item Apply a second order high pass Butterworth filter with cutoff frequency 0.04 to get rid of the low frequency noise/trend. This frequency was determined emperically.
\end{itemize}
The net result is shown in Figure \ref{fig:zeroinputsystem}. Here we see indeed that the data has been detrended and that the low frequencies have been suppressed. The peaks are cut off although one peak seems to remain. 

\begin{figure}
	\centering
	\subfloat[The original output when we apply a zero input of length 1000.]{\includegraphics[width=0.5\linewidth,height=0.2\linewidth]{../plots/zeroinputoriginal}\label{fig:zeroinputoriginal}}
	\subfloat[The trend of the low frequency noise. Determined by a low-pass second order Butterworth filter.]{\includegraphics[width=0.5\linewidth,height=0.25\linewidth]{../plots/zeroinputtrend}\label{fig:zeroinputtrend}}
	
	\subfloat[The output of the zero input, after subtracting the mean, peak shaving, detrending and high-pass filtering.]{\includegraphics[width=0.5\linewidth]{../plots/zeroinputsystem}\label{fig:zeroinputsystem}}
\end{figure}
	
\subsection{Impulse response}
The impulse response can be used to get an estimate of the total delay in the system. In theory, the impulse response contains all the information of the system, but due to random initial conditions and noise on the output, measuring the impulse response only once carries no information whatsoever. To solve this problem, we can calculate the impulse response 2000 times and average the result, without preprocessing. This way, we assume that the first couple of values will be almost equal to zero and hence this will be the delay of the system. By averaging, we also compensate for the random initial condition. The numerical results are shown in Figure \ref{fig:impulsedelay}. Note that in these experiments, we choose the amplitude of the impulse response to be 3, because if the amplitude is too small, the noise will dominate the system response.\\
Another way of estimating the delay is by using the MATLAB \texttt{cra} function. This returns the 99\% confidence intervals. This is shown in Figure \ref{fig:cra_delay}. Here we also notice that the estimated  delay by MATLAB is 17, a confirmation of our own result.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{../plots/impulsedelay}
	\caption{The average of 2000 times measuring the impulse response of the system. The delay seems to be about 17 timesteps.}
	\label{fig:impulsedelay}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../plots/cra_delay2}
\caption{The estimated delay of the system with 99 percentile confidence intervals in MATLAB. Also note that the delay is estimated to be 17, as our own results.}
\label{fig:cra_delay}
\end{figure}

\subsection{Experimental Bode plot}

Another way of getting to know the system is by experimentally creating a bode plot. This can be achieved in the following way. If we apply a sine function $A \sin(\omega t)$, where $\omega$ is the frequency, we expect something back of the form $A'\sin(\omega t + \phi)$ with a different amplitude $A'$ and phase $\phi$. For the frequencies, we chose normalized frequences $\omega \in [0, \pi]$.
The bode plot value at the given frequency $f$ then is $\frac{A'}{A}.$ 
Also note that we averaged the output of the system over 50 runs in order to minimize the effects of the noise on the output.

A more practical way of calculating the bode plot can be achieved by taking the FFT of the input and output signal. The output signal will also contain a peak at the frequency $\omega$ such that we can just divide the maximum of the FFT of the output signal by the maximum of the FFT of the input signal.
A more robust way, which is less prone to noise on the output peak, of finding the Bode plot, is not dividing the maximum values of the FFT's but the standard deviations. This works because the standard deviation of the FFT will be proportional to the maximum value because the filtered noise does not bear high magnitude components in the signal anymore. 

Both practical methods seemed to generate about the same result, shown in Figure \ref{fig:bodeexperimental}. On the figure, we clearly see two peaks that correspond with the true system dynamics plus another small peak around $\omega=1.6$. Note that this plot has also been smoothened using the MATLAB \texttt{smooth} command. For very low frequencies, we see that the system behaves as a second order high pass filter, which is expected because we filtered the output with second order Butterworth filter. For high frequencies, the amplitude plot also decreases again to about -40dB, which apparently corresponds to the noise.

An identified model should also behave as much as possible like this Bode plot and as we will see, this will mostly be the case as well. We can however not expect all models to follow this curve exactly due to the noise present on the curve as well.

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{../plots/exp_bode}
\caption{An experimental Bode plot of the system computed by applying exact sine waves with a given frequency and then dividing the maximum of the output FFT and input FFT. The same result can be obtained by dividing the standard deviations of the output and input FFT.}
\label{fig:bodeexperimental}
\end{figure}

\section{Identification and Validation }
We now turn to setting up actual experimental models for our unknown system. Now that we know the behaviour of the noise, we can preprocess the data appropriately, as described in the previous section. Identifying the system is the next step. We proceed by first estimating the best ARX model, then using the values of the optimal parameters to continue with ARMAX to get a better noise estimation. Afterwards we also proceed to OE, Box-Jenkins and subspace models. We will also validate the models by looking at an independent validation data set and by analyzing the residuals.
We hope of course that the fit percentage on our validation data will increase with the complexity of the model. We will also check if it is possible to perform model order reduction by looking at the Hankel singular values and the pole-zero map for cancellations and do a residual analysis.

\subsection{ARX Estimation}
ARX is one of the least complex models for system identification. It assumes the following input-output relation
\[
y(k) = \frac{B(q)}{A(q)}u(k) + \frac{1}{A(q)}e(k),
\]

where the input and the noise $e(k)$ have the same system behavior. The advantage of this system is that it is linear in the parameters and hence can be computed really fast. The disadvantage is that the noise and the input have the same poles and hence the same system behavior, which is not realistic.


To make estimations of systems, we will mainly use pseudobinary random numbers with an amplitude of three, as these are good generators for white noise. All frequencies will then be present in the input signal. This is a lot better than for example only applying an impulse, because this generates not enough energy.


\subsubsection{Choosing an ARX model using \texttt{arxstruc}}
If we choose ranges 1:20 for $n_a$ and $n_b$ (the orders of the numerator and denominator) and 16:18 for the delay, we get the following figures using \texttt{arxstruc} in Figure \ref{fig:arxstruc}. Note that this figure may depend on the exact input and this may vary from run to run. The green bar is called the `MDL' choice and we will also work further on this one, because the so-called \emph{best} fit has impractical large orders. The (little) extra fit you get has no more weight than the simplicity of the model. Certainly not if you want to use your model later on. The parameter values were $n_a = 9$, $n_b = 16$ and the delay 17. The fit percentage on the estimation data was 59.53\%.
Figure \ref{fig:arxbode} shows the Bode plot of this model, together with the experimental Bode plot from the previous section. It is clearly visible that the estimated model beautifully follows the estimated Bode plot. The only disadvantage is that there seems to be a 2dB error, which can be explained by the random noise and the behavior at low frequences. For low frequencies the identified model is higher then the FFT estimation. This is due to the fact that our  FFT estimation for these low frequencies is no longer reliable because we heavily preprocessed the data there. For high frequencies, there are extra poles and zeros in the identified model, this could be a sign of overmodeling.\\
We also plotted the 99\% confidence interval around the Bode plot of the ARX system, but our experimental Bode plot of the first section did not completely fit in these intervals. This may be due to a small error in the generation of our experimental plot, but we were not able to explain this thoroughly.


\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{../plots/arxstruc}
\caption{The \texttt{arxstruc} plot for choosing an optimal ARX model.}
\label{fig:arxstruc}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{../plots/bode_arx_9_16_exp}
\caption{The Bode plot of the estimated ARX model with orders $n_a=9$ and $n_b=17$ in blue and the estimated Bode plot in magenta. They obey the same system behavior. Only for low and high freqencies there is a small difference due to the filtering and the noise.}
\label{fig:arxbode}
\end{figure}

\subsubsection{Validating the ARX model}
There are a couple of different ways to validate the estimated ARX model. First we can look how well the model works on the validation data. Here the fit is 61\% which is as good as for the estimation data. Figure \ref{fig:arxresidual} shows the autocorrelation of the residue between the modeled output of the identified system and the exact output of the system. We want this to be white noise, meaning that we cannot model the system better. Otherwise we would be modeling noise and not the system. Here the error seems to be white indeed because the autocorrelation is approximately a discrete delta function.
Also on Figure \ref{fig:arxresidual} on the right, the input of the system is not correlated with the error, it is zero within 99\% confidence intervals, meaning that there is no unexplained part of the error that can be explained by the input.

\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{../plots/auto_corr_ARX_9_16}
\caption{The autocorrelation of the error between the estimated and true outputs of the system in the left figure. This error seems to be white. The right figure shows the cross-correlation between this error and the input signal.}
\label{fig:arxresidual}
\end{figure}

Figure \ref{fig:pzmap} shows the poles and zeros of our estimated model. It is of course a stable model as the poles are within the unit circle. Also note that there are a few poles and zeros that lie quite close to each other, meaning that these may be cancelled by each other without giving in too much on the error (they are just there to obey your order restriction).
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{../plots/pzmap_ax_9_16}
\caption{Pole-Zero map of the identified ARX model. The system is stable but there are opportunities to perform model order reduction by pole-zero cancellation.}
\label{fig:pzmap}
\end{figure}


\subsubsection{Model Order Reduction}
Of course, the pole-zero plot is not enough to perform model order reduction. Another approach is looking at the Hankel singular values, shown in Figure \ref{fig:arxhankel}. We see that the singular values decay quite quickly. Now we can truncate the model up to the number of singular values of our choice.  We choose to maintain ten singular values, to have enough compression in our system, but while keeping the approximation error small.
\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{../plots/hankel_arx_9_16}
\caption{The Hankel singular values of the identified ARX. We chose to truncate the system up to 10 terms because the error is small when we truncate there.}
\label{fig:arxhankel}
\end{figure}

To validate this model, we notice, using the MATLAB \texttt{compare} function, that the fit on the validation data is 57.48\%. This is a little less than the error by the unreduced model, which is a logical result because the reduced model lies not far from the original ARX model.

The Bode plot of this reduced model is shown in Figure \ref{fig:arxbodereduced}, together with the estimated Bode plot from the first section. Here again we note that it clearly follows the system dynamics in the two peaks (and a bit in the third), but seems to be off by 2dB or so just as the first ARX model. Also, for higher frequencies, the fit of the reduced model seems to be even better than the identified unreduced ARX model. This is due to the fact that the original ARX model has more poles and zeros that mainly play around these higher frequencies.

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../plots/bode_arx_9_16_compare}
\caption{The estimated Bode plot from the first section in magenta, the Bode plot of the ARX model in blue and the Bode plot of the reduced ARX model in red. They all seem to have the same system dynamics.}
\label{fig:arxbodereduced}
\end{figure}

\subsection{ARMAX Estimation}
The generated ARX model seems to attain a fit of about 60\% on estimation and validation data. We hope that by adding extra complexity to the model, we can generate a better fit. The ARMAX model is given by
\[
y(k) = \frac{B(q)}{A(q)}u(k) + \frac{C(q)}{A(q)}E(k).
\]
We are therefor adding extra complexity to the noise term. A disadvantage of this method is that is not linear in the parameters anymore and this requires an optimization routine each step, instead of linear system of equations as in ARX. The similarity with ARX is that the input and noise have the same poles, which is not completely realistic.

There is no similar function as \texttt{arxstruc} for ARMAX due to the higher complexity and runtime of ARMAX. A good initial estimate of the order $n_a$, $n_b$ and $n_c$ is thus necessary!
For this estimate, we will use the same $n_a$ and $n_b$ as for the ARX model and iterate over the $n_c$ parameter. This is mainly because we have no a priori knowledge about this $n_c$ parameter.

\subsubsection{Analysis of the best ARMAX model}
If we choose $n_c$ between 1 and 20 and fix $n_a=9$ and $n_b=16$, then the optimal value seems to be 14. The fit on the estimation data is 61,14\%, which is better than the ARX model, while the fit on the validation data is 62.32\%. This is an improvement of 100 base points over ARX.

The Bode plot of ARMAX together with ARX is shown in Figure \ref{fig:armaxbode}. Notice that ARX and ARMAX have the same system behavior in the three visible peaks, but ARMAX follows the estimated Bode plot better than ARX in the lowest frequencies. This is mostly because the noise has a large impact for small frequencies and ARMAX is better than ARX in modeling the noise impact. For higher frequences, the effects of ARX and ARMAX are mostly the same.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{../plots/bode_armax}
\caption{Bode plot of the ARMAX model with $n_c = 14$ together with the ARX Bode plot and the estimated Bode plot.}
\label{fig:armaxbode}
\end{figure}

The residual analysis in Figure \ref{fig:residualarmax} shows that the error between the estimated output using ARMAX and the true output of the system is again uncorrelated and forms a delta function. This is a good result. On the plot on the right we also see that the output error is mostly uncorrelated with the input. In general this means that our ARMAX model explained most of the output by the input.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{../plots/auto_corr_armax_9_16_13}
	\caption{The residual analysis of the ARMAX model.}
	\label{fig:residualarmax}
\end{figure}

The pole-zero map in Figure \ref{fig:armaxpolezero} indicates that there are at least four couples of poles and zeros that lie close to each other. The complexity of the model can hence be reduced by model order reduction.

\begin{figure}
	\centering
	\subfloat[Pole-Zero map of the ARMAX model. Notice that there are a couple of poles and zeros close to each other, which are perfect for model order reduction.]{\includegraphics[width=0.5\linewidth]{../plots/pzmap_armax_9_16_13}\label{fig:armaxpolezero}}
	\subfloat[The pole-zero map of the tenth order ARMAX model.]{\includegraphics[width=0.5\linewidth]{../plots/pzmap_armax_10_10_16}\label{fig:armax10reduced}}
\end{figure}

\subsubsection{Model order reduction on ARMAX}
Remember that we truncated the previous ARX model to order 10 and that the results were rather similar to the true identified ARX model. We will now setup an ARMAX model where $n_a$ and $n_b$ are both equal to 10, and choose the best $n_c$. This value is calculated to be 14.
The fit on the estimation data is 60.34\% and 61.89\% on the validation data. These are both a little bit less than ARMAX 9 and 16, but this is numerically almost equal. There are no big differences apparently between these two models.
For completeness we show the pole-zero map of this tenth order system in Figure \ref{fig:armax10reduced} and notice that there are no poles and zeros close to each other.


We could also have reduced the first ARMAX model to get rid of the pole-zero cancellation in Figure \ref{fig:armaxpolezero}. We choose however te continue working with the reduced ARX model and see how well ARMAX fits here. As mentioned before, this won't make much difference as most of these methods yield about the same result.

\subsubsection{Conclusion of ARMAX}
The only difference between ARX and ARMAX is the extra numerator in the noise term. Hence we didn't expect ARMAX to truely outperform ARX. The main difference between the two is the behaviour for low frequencies on Figure \ref{fig:armaxbode}.
Also, just as for ARX, the reduced model captures about the same behaviour as ARX and ARMAX, with a significantly lower order. Note however that we did not fully calculate a model reduction of our ARMAX model, but only used the orders of the reduced ARX model and plugged these into ARMAX.

\subsection{OE Estimation}
The next model is OE (`Output Error'). This method tries to model a system using the following model:
\[
y(k) = \frac{B(q)}{F(q)}u(k) + e(k).
\]
We see that OE makes no attempt to model the noise dynamics of the system (it assumes additive noise) and only focusses on the input-output relation. Hence we expect that OE will perform slightly worse. The only free parameters in the system are the polynomial degrees $n_b$ and $n_f$.

\subsubsection{Finding a good OE Model}
For ARX and ARMAX we used a model with $n_a=9$ and $n_b=16$. These gave a relatively good fit of the data. If we continue with these numbers, now we have to take $n_f = 9$. The fit on the estimation data is 56.25\% and on the validation data 58.02\%. Here we immediately notice that this is a little worse than either ARX and ARMAX. The Bode plot for this is shown in Figure \ref{fig:bodeoe}. Notice that the OE model follows the first two peaks in the Bode plot well, but not the third one. For low frequencies, the OE model is too high. This follows the same conclusions as for ARX because we have little noise modelling in OE and hence we cannot follow the effects of the low frequency noise.

We also plotted the Bode plot of the OE model where we used $n_f = n_b = 10$ in line with the previous models. This follows the same behaviour for normal frequencies, but is too far off for high frequencies. We will not consider it anymore. It does however follow the third peak in the Bode plot better than the other OE model.

\subsubsection{Validating the OE Model}
The residual analysis in Figure \ref{fig:oeresidual} shows that the error almost quite behaves like white noise, but there are a few components around zero that do not lie in the 99\% confidence bounds of zeros. This may be an indication that OE misses a little bit of information because we have no impact on the noise terms. The figure on the right also says that this cannot be changed by modelling the input-output relation better because the error is uncorrelated with the input. This is hence a shortcoming of OE.

The pole-zero map of this estimated system in Figure \ref{fig:polezerooe} shows that there are a couple of poles and zeros that lie in each others vicinity and may be cancelled.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{../plots/bode_oe}
\caption{Bode plot of the fitted OE model with orders $n_f=9$ and $n_b=16$.}
\label{fig:bodeoe}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{../plots/auto_corr_oe_16_9}
\caption{The residual analysis of the OE model with orders $n_f=9$ and $n_b=16$. We notice that the error between the fitted output of this model and the true output of the system does not quite behave like white noise.}
\label{fig:oeresidual}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{../plots/pzmap_oe_16_9}
\caption{The pole-zero map of the OE model with orders $n_f=9$ and $n_b=16$. There are at least three pole-zero pair that may be cancelled by each other.}
\label{fig:polezerooe}
\end{figure}

\subsubsection{Model order reduction of the OE model}
Lets now perform model order reduction on the calculated OE Model. The Hankel singular values are shown in Figure \ref{fig:hankeloe}. Possible places to truncate are after 8 or 12 singular values. Here we chose to use 8. The Bode plot of this reduced model is shown in Figure \ref{fig:oereduced}.
\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{../plots/hankel_oe_16_9}
\caption{The Hankel singular values of the OE reduced model with orders $n_f=9$ and $n_b=16$. }
\label{fig:hankeloe}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{../plots/bode_oe_trunc}
\caption{The Bode plot of the reduced OE model to order 8.}
\label{fig:oereduced}
\end{figure}
We notice that the behaviour of this reduced model is far from optimal. For high frequencies, the Bode plot of the reduced model is far of from the estimated Bode plot and it also does not follow the third peak in this estimated Bode plot.

\subsubsection{Conclusion on OE}
The OE Model on this problem behaves worse than the ARX and ARMAX models. The fit percentages are lower on both the estimation and validation data, and the Bode plots also do not seem to follow all the system dynamics. We also noticed on the residual analysis that the error is not completely white, meaning that OE misses a small amount of information. This can be explained by the fact that it just assumes additive noise without making any attempt to improve the noise behavior.

\subsection{Box-Jenkins Estimation}
The next model we consider in this report is the Box-Jenkins model. This tries to model the system with the following model relation:
\[
y(k) = \frac{B(q)}{F(q)}u(k) + \frac{C(q)}{D(q)}e(k).
\]
Here the input of the system and the noise are modelled seperately and should hence deliver better results than all previous methods. The downside is that we have a lot more degrees of freedom and this will take more time to compute the optimization routine.

\subsubsection{Finding a good Box-Jenkins model}
We now have a lot more degrees of freedome $n_f, n_b, n_c$ and $n_d$. It is not good approach to loop over all these values and pick the best one. A more pragmatic approach consists in reusing some of these values from the ARX or ARMAX models. Let us now take $n_b = 16$ as in ARX,, $n_c=14$ as in ARMAX, $n_f = n_d = 9$ just as in ARX.
The estimation fit is 60.92\% and the validation fit 61.88\%. This is about the same as for ARMAX. This is an indication that Box-Jenkins is not able to find a better model for the system than ARMAX and ARX. The value of 60\% seems the maximum attainable by the estimation techniques we have seen.

The Bode plot is shown in Figure \ref{fig:jenkinsbode}. The fit is not that good. For normal frequencies, Box-Jenkins is not able to follow the third peak in the data well. Also there is a big spike around 2.6. It seems that our Box-Jenkins suffers from overmodelling problems.
\begin{figure}
\centering
\includegraphics[width=\linewidth]{../plots/bode_bj}
\caption{Bodeplot of the Box-Jenkins method with $n_f=9, n_b=16, n_c=13, n_d=9$. It tries the follow the system dynamics but is not good for high frequencies.}
\label{fig:jenkinsbode}
\end{figure}
We also tried the Box-Jenkins method with $n_b = n_c = n_d = n_f = 10$ in line with the model order reduction of ARX, but the Bode plot suffered from the same symptoms as our first Box-Jenkins method.
This is a rather strange result as we would expect that Box-Jenkins would at least perform as good as ARX because it has more degrees of freedom in the coefficients.

\subsection{Subspace methods for system identification}
The final method we try is subspace identification. The only parameter here is the delay of the system, which is 17. The Bode plot is shown in Figure \ref{fig:bodesub}.The fit on the estimation data is 61\% and on the validation data 62.27\%. This is in the same line of the other methods.
\begin{figure}
\centering
\includegraphics[width=\linewidth]{../plots/bode_sub}
\caption{Bode plot of the identified system using Subspace identification with delay 17.}
\label{fig:bodesub}
\end{figure}
The Bode plot follows the experimental Bode plot smoothly and also follows the third peak in the plot. There is however a very sharp spike at frequency 3. This is an indication that there is a 
pole too many. The pole-zero map of this identified system in Figure \ref{fig:pzmapsub}. There are indeed two zeros and poles very close to -1 that correspond to the high frequencies.

\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{../plots/pzmap_sub}
\caption{Pole-zero map of the subsystem identification tool.}
\label{fig:pzmapsub}
\end{figure}

\subsubsection{Model order reduction}
The first order for which these zero/pole pares disappear is for order 11. The Bode plot for this system is shown in Figure \ref{fig:subreduced}. Notice that indeed the spike at frequency 3 is gone, but the reduced system does not follow our estimated Bode plot at all for higher frequencies. In this example model order reduction does not seem to be a good idea.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../plots/bode_sub_red}
\caption{Bode plot for the reduced model to order 11 by subspace identification.}
\label{fig:subreduced}
\end{figure}

\section{Conclusion}
In this report we tried to estimate an unknown system that is prone to high levels of noise. We proceeded by first getting to know the system by applying a couple of easy inputs such as the zero input, an impulse, sine waves for a Bode plot, \dots . From this we were able to characterize the noise behaviour, get an idea of the delay in the system and calculate an experimental Bode plot. From this plot, the system dynamics contained three peaks.
We then moved on to identification techniques. First we tried ARX and this already gave a quite good fit on the esimation and validation data. Model order reduction could be done to make the model less complex with almost the same result. The next step was ARMAX to try to model the system dynamics of the noise better. This was indeed the case for the low frequency noise in the system.
The following model was OE. The fit of this model was a little worse than ARX because it is less complex. We did not go in too much detail on this model.
Afterwards we turned to Box-Jenkins which is a lot more complex, but this also did not improve the fit on the data and Bode plot much. This all indicates that a fit of around 60\% is the maximum attainable for this model.
For completeness wel also looked at subspace methods. This seemed to model the Bode plot quite well (one of the best models), but there are a couple of unexplained peaks in the modeled system.
\end{document}